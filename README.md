# SOLID
* S — Single Responsiblity Principle (Princípio da responsabilidade única)
* O — Open-Closed Principle (Princípio Aberto-Fechado)
* L — Liskov Substitution Principle (Princípio da substituição de Liskov)
* I — Interface Segregation Principle (Princípio da Segregação da Interface)
* D — Dependency Inversion Principle (Princípio da inversão da dependência)

## RESUMO

### Coesão
* Uma classe coesa faz bem uma única coisa
* Classes coesas não devem ter várias responsabilidades

### Encapsulamento
* Getters e setters não são formas eficientes de aplicar encapsulamento
* É interessante fornecer acesso apenas ao que é necessário em nossas classes
* O encapsulamento torna o uso das nossas classes mais fácil e intuitivo

### Acoplamento
* Acoplamento é a dependência entre classes
* Acoplamento nem sempre é ruim, e que é impossível criar um sistema sem nenhum acoplamento
* Devemos controlar o nível de acoplamento na nossa aplicação (falaremos mais sobre isso)

### O Princípio de Responsabilidade Única (SRP)
Uma classe deve ter um e apenas um motivo para ser alterada;

### O Princípio Aberto/Fechado (OCP)
Diz que um sistema deve ser aberto para a extensão, mas fechado para a modificação
Isso significa que devemos poder criar novas funcionalidades e estender o sistema sem precisar modificar muitas classes já existentes

### Princípio de Substituição de Liskov (LSP)
Diz que devemos poder substituir classes base por suas classes derivadas em qualquer lugar, sem problema.

### Princípio de Inversão de Dependência (DIP)
Diz que implementações devem depender de abstrações e abstrações não devem depender de implementações;

### Princípio de Segregação de Interfaces (ISP)
Diz que uma classe não deve ser obrigada a implementar um método que ela não precisa;